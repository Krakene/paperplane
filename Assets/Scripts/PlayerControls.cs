﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.EventSystems;

public class PlayerControls : MonoBehaviour
{
    Rigidbody2D rb;
    [SerializeField] float force = 20f;
    [SerializeField] float speed = 10f;

    private void Start() {
        rb = transform.GetComponent<Rigidbody2D>();
    }

    private void Update() {
        if(Input.touchCount > 0 ){
            Touch touch = Input.GetTouch(0);

            if(touch.phase == TouchPhase.Stationary){
                rb.AddForce(transform.up * force);
            }
        }

        rb.velocity = new Vector2(speed, rb.velocity.y);
    }

    public void Crash(){
        transform.gameObject.SetActive(false);
        Debug.Log("CRASH");
    }
}
