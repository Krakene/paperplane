﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class CameraMouvements : MonoBehaviour
{
    [SerializeField] private GameObject player;
    [SerializeField] private float timeOffset;
    [SerializeField] private float posOffset;


    private Vector3 velocity;

    // Update is called once per frame
    void Update()
    {
        Vector3 newPos = transform.position;
        newPos.x = player.transform.position.x + posOffset;
        transform.position = Vector3.SmoothDamp(transform.position, newPos, ref velocity, timeOffset);
    }
}
