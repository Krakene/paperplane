﻿using UnityEngine;
using System.Collections;

public class SpawnObstacle : MonoBehaviour
{
    [SerializeField] GameObject[] obstacles;
    [SerializeField] float spawTimes;
    bool spawn = false;

    void Start()
    {
        StartCoroutine(SpawnObs());
    }

    IEnumerator SpawnObs()
    {
        if(!spawn){
            yield break;
        }
        yield return new WaitForSeconds(spawTimes);
        GameObject obs  = obstacles[Random.Range(0, obstacles.Length - 1)];
        Instantiate(obs, transform.position, Quaternion.identity);
        StartCoroutine(SpawnObs());
    }

}
