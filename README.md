# Paper Plane

## Description
Paper plane is a clone of Flappy bird made with Unity where you have to click directly on the plane to make it go up and avoid obstacles.

## Requirements
Unity version 2019.4.25 or higher

## Getting Started
Clone or download the repository
Open the project in Unity
Play the scene in the Unity Editor

## Controls
Click on the plane to make it go up.

## Assets
Images and graphics in this game were created by me and may not be used for any other purposes without our express permission.